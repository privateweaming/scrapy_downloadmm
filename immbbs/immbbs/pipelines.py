# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import hashlib
import scrapy
from scrapy.pipelines.images import ImagesPipeline
from scrapy.utils.python import to_bytes


class ImmbbsPipeline(object):
    def process_item(self, item, spider):
        return item


class MyImagesPipeline(ImagesPipeline):
    def get_media_requests(self, item, info):
        for image_url in item['image_urls'][0]:
            yield scrapy.Request(image_url, meta={'directory': item['image_urls'][1]})

    def file_path(self, request, response=None, info=None):
        if not isinstance(request, scrapy.Request):
            url = request
        else:
            url = request.url

        image_guid = hashlib.sha1(to_bytes(url)).hexdigest()[:10]
        if 'directory' in request.meta:
            return 'full/%s/%s.jpg' % (request.meta['directory'], image_guid)
        else:
            return 'full/%s.jpg' % image_guid
