# -*- coding: utf-8 -*-
import re
import scrapy


class ImagesSpider(scrapy.Spider):
    name = "images"
    allowed_domains = ["www.immbbs.com"]
    start_urls = ['http://www.immbbs.com/list-24-1.html']

    def parse(self, response):
        shows_links = response.css('li.Pli a::attr(href)').extract()
        for link in shows_links:
            link = response.urljoin(link)
            yield scrapy.Request(link, callback=self.extract_images)

        for page in response.css('#pageNum a::attr(href)').extract():
            page = response.urljoin(page)
            yield scrapy.Request(page, callback=self.parse)

    def extract_images(self, response):
        images = response.css('.content img::attr(src)').extract()
        images = filter(lambda x: 'uploadfile' in x.lower(), images)
        images = map(lambda x: x.replace('pic.yiipic.com', 'mtl.ttsqgs.com'), images)
        map(self.log, images)

        # save to special directory, instead of just hash of url
        dir_name = response.css('title::text').extract_first()
        dir_name = re.findall(r'\[.+\]\s*(.+P\])', dir_name)
        dir_name = (dir_name and dir_name[0] or 'other').replace('/', '-')
        self.log(dir_name)
        yield {
            'image_urls': (images, dir_name)
        }

        for page in response.css('#pages a::attr(href)').extract():
            if 'show' in page:
                page = response.urljoin(page)
                yield scrapy.Request(page, callback=self.extract_images)
